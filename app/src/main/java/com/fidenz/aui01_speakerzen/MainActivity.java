package com.fidenz.aui01_speakerzen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chootdev.blurimg.BlurImage;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // blurred background
        BlurImage.withContext(this)
                .setBlurRadius(9.5f)
                .setBitmapScale(0.1f)
                .blurFromResource(R.drawable.speaker_grill)
                .into((ImageView) findViewById(R.id.bgImgView));

        // 'Sign up now!' text colour
        TextView txtView = (TextView) findViewById(R.id.txtSignUp);
        String s = getResources().getString(R.string.sign_up);
//        txtView.setText(Html.fromHtml(s, Html.FROM_HTML_OPTION_USE_CSS_COLORS)); // not supported in API level < 24. Find a workaround later.
        txtView.setText(Html.fromHtml(s));

    }

    public void onButtonPressed(View view) {

        String tag = view.getTag().toString();

        switch (tag) {
            case "TWITTER_BTN" : {
                AppToast.showToast(getApplicationContext(), AppToast.MESSAGES.TWITTER);
            } break;
            case "LINKEDIN_BTN" : {
                AppToast.showToast(getApplicationContext(), AppToast.MESSAGES.LINKEDIN);
            } break;
            case "FB_BTN" : {
                AppToast.showToast(getApplicationContext(), AppToast.MESSAGES.FB);
            } break;
            case "SIGN_IN_BTN" : {
                AppToast.showToast(getApplicationContext(), AppToast.MESSAGES.SIGNED_IN);
            } break;
            case "SIGN_UP" : {
                AppToast.showToast(getApplicationContext(), AppToast.MESSAGES.SIGN_UP);
            } break;

        }

    }
}
