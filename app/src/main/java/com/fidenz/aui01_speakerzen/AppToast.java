package com.fidenz.aui01_speakerzen;

import android.content.Context;
import android.widget.Toast;

public final class AppToast {

    public enum MESSAGES {
        FB,
        TWITTER,
        LINKEDIN,
        SIGNED_IN,
        SIGN_IN_ERROR,
        SIGN_UP
    }

    // duration of the toast in milliseconds
    private static int duration = 2500;

    // invokes the toast building method by passing the correct message
    public static void showToast(Context context, MESSAGES m) {

        switch (m) {
            case FB: {
                String msg = "Facebook";
                buildToast(context, msg, duration);
            } break;
            case TWITTER: {
                String msg = "Twiiter";
                buildToast(context, msg, duration);
            } break;
            case LINKEDIN: {
                String msg = "LinkedIn";
                buildToast(context, msg, duration);
            } break;
            case SIGNED_IN: {
                String msg = "Signed In";
                buildToast(context, msg, duration);
            } break;
            case SIGN_IN_ERROR: {
                String msg = "Error! Try again.";
                buildToast(context, msg, duration);
            } break;
            case SIGN_UP: {
                String msg = "Sign up";
                buildToast(context, msg, duration);
            } break;
        }

    }

    // build and show the toast to the user
    private static void buildToast(Context context, CharSequence message, int duration) {

        Toast.makeText(context, message, duration).show();

    }
}
